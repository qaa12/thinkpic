ActiveAdmin.register PicStyle do
 form :html => { :enctype => "multipart/form-data" } do |f|
   f.inputs "Details" do
    f.input :main_color
    f.input :bold_color
    f.input :name
    f.input :bg, :as => :file
  end
  f.buttons
 end
 index do
    column :bg do |x|
	    raw "<h2>#{x.name}</h2><div style='width:440px; height:250px;background-image: url(\"#{x.bg.url}\")'>
	    			<h1 style='color:#{x.main_color}'>#{x.main_color}</h1>
	    			<h1 style='color:#{x.bold_color}'>#{x.bold_color}</h1>
	    			</div>"
	  end
	  default_actions
  end
end
