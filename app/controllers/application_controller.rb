# encoding: utf-8

class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :sign_out_if_banned



  private


    def sign_out_if_banned
      if user_signed_in?
        if current_user.banned == true
          sign_out(:user)
          flash[:error] = "Уважаемый пользователь, поздравляем тебя с баном! Скажи привет френдзоне или напиши в техническую поддержку. Вопросы типа WTF, не принимаются."
          redirect_to root_path
        end
      end
    end

end

