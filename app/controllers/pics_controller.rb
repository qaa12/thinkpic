class PicsController < ApplicationController

  before_filter :authenticate_user!, :only => [:new, :create, :destroy]
  require 'will_paginate/array'

	def index
	  @pics = Pic.all.find_all{|x| x.likes.count >= 5}.paginate(:page => params[:page], :per_page => 10)
	end

    def list
	  @pics = Pic.find(:all, order:"created_at DESC").paginate(:page => params[:page], :per_page => 10)
	end


	def new
		@pic = Pic.new
        @pic_style = PicStyle.find_by_id (params[:style] || 1)
        respond_to do |format|
          format.html # index.html.erb
          format.json {render json: @pic_style.to_js}
        end
	end

	def create
		@pic = Pic.new(params[:pic])
        @pic.user = current_user
		respond_to do |format|
			if @pic.save
				format.html{redirect_to pic_path(@pic)}
			else
				render action: "new"
			end
		end
	end

    def show
        @pic = [] << (Pic.find_by_id params[:id])
    end

    def userpics
        @user = User.find_by_id(params[:username])
        @pics = @user.pics.paginate(:page => params[:page], :per_page => 10)
    end

    def pics_by_tags
        @pics = Pic.tagged_with(params[:tag]).paginate(:page => params[:page], :per_page => 10)
    end

    def likeit
        @pic = Pic.find_by_id params[:id]
        if !(like = @pic.likes.where(user_id: current_user)).blank?
            Like.destroy like
        else
            Like.create(pic_id: @pic.id, user_id: current_user.id)
        end
    end

    def destroy
        pic = Pic.find_by_id params[:id]
        if pic.user == current_user
            pic.destroy
            redirect_to root_path
        else
            redirect_to root_path
            flash[:notice] = "hack"
        end
    end

end
