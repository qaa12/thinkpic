class PicStyle < ActiveRecord::Base
  attr_accessible :main_color, :bold_color, :bg, :name
  has_attached_file :bg
  has_many :pic

  def to_js
  	{
  		"main_color" => main_color,
  		"bold_color" => bold_color,
  		"bg" => bg.url
  	}
  end

end
