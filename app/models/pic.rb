class Pic < ActiveRecord::Base
	belongs_to :user
  has_many :likes
  belongs_to :pic_style
	attr_accessible :main, :bold, :tag_list, :pic_style_id
  acts_as_taggable_on :tags
  default_scope order("created_at DESC")
end
