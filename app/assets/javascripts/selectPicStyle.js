 $(document).ready(function() {
    $('#pic_pic_style_id').change(function(){
    	$.getJSON(location.href+"?"+'style='+$('#pic_pic_style_id').val(), function(data) {
    		$('.pic').css("background-image", "url("+data.bg+")");
    		$('.main_text').css("color", data.main_color);
    		$('.bold_text').css("color", data.bold_color);
    	});
    });
  });
 