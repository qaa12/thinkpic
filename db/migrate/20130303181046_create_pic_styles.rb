class CreatePicStyles < ActiveRecord::Migration
  def change
    create_table :pic_styles do |t|
    	t.string :main_color
    	t.string :bold_color
    	t.string :bg_file_name
    	t.string :bg_content_type
    	t.integer :bg_file_size
      t.timestamps
    end
  end
end
