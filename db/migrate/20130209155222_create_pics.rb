class CreatePics < ActiveRecord::Migration
  def change
    create_table :pics do |t|
      t.string  :main
      t.string  :bold
      t.integer :user_id
      t.timestamps
    end
  end
end
