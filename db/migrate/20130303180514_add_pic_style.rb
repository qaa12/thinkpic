class AddPicStyle < ActiveRecord::Migration
  def up
		add_column :pics, :pic_style_id, :integer, :default=> 5
		Pic.all.each do |p|
      p.update_attribute :pic_style_id, 5
    end
  end

  def down
  end
end
